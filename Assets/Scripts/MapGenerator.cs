﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public Transform obstaclesHolder;

    public float renderDistance = 20f;

    public float startDifficulty = 0f;
    public float maxDifficulty = 90f;
    public float difficultyChangeSpeed = 0.1f;
    public float startObstacleDistance = 20f;
    public float minObstacleDistance = 5f;

    public GameObject[] collectibles;
    public Obstacle[] easyObstacles;
    public Obstacle[] mediumObstacles;
    public Obstacle[] hardObstacles;

    private float currentDifficulty;
    private float currentObstacleDistance;

    private float currentPosition;
    private float nextObstaclePosition;

    private void Awake()
    {
        currentDifficulty = startDifficulty;
        currentObstacleDistance = startObstacleDistance;
        nextObstaclePosition = startObstacleDistance;
    }

    private void Update()
    {
        currentPosition = Camera.main.transform.position.x;
        currentDifficulty = Mathf.Clamp(currentDifficulty + difficultyChangeSpeed * Time.deltaTime, 0, maxDifficulty);
        currentObstacleDistance = (1 - currentDifficulty / maxDifficulty) * (startObstacleDistance - minObstacleDistance) + minObstacleDistance;

        if (currentPosition + renderDistance >= nextObstaclePosition)
            SpawnNextObstacle();
    }

    private void SpawnNextObstacle()
    {
        int obstacleIndex = 0;
        switch ((int)(currentDifficulty / (maxDifficulty / 3)))
        {
            case 0: //Easy
                obstacleIndex = UnityEngine.Random.Range(0, easyObstacles.Length);
                SpawnObstacle(easyObstacles[obstacleIndex]);
                break;
            case 1: //Medium
                obstacleIndex = UnityEngine.Random.Range(0, mediumObstacles.Length);
                SpawnObstacle(mediumObstacles[obstacleIndex]);
                break;
            case 2: //Hard
            case 3:
                obstacleIndex = UnityEngine.Random.Range(0, hardObstacles.Length);
                SpawnObstacle(hardObstacles[obstacleIndex]);
                break;
            default:
                break;
        }
    }

    private void SpawnObstacle(Obstacle obstacle)
    {
        Vector2 spawnPosition = new Vector2(nextObstaclePosition + obstacle.obstacleLength / 2f, 0);
        Instantiate(obstacle, spawnPosition, Quaternion.identity, obstaclesHolder);

        nextObstaclePosition += currentObstacleDistance + obstacle.obstacleLength;

        spawnPosition += Vector2.right * currentObstacleDistance / 2f + Vector2.up * UnityEngine.Random.Range(-2f, 2.5f);
        GameObject collectible = collectibles[UnityEngine.Random.Range(0, collectibles.Length)];
        Instantiate(collectible, spawnPosition, Quaternion.identity, obstaclesHolder);
    }
}
