﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{

    public float movementSpeed = 1;

    private void FixedUpdate()
    {
        transform.position += Vector3.right * movementSpeed * Time.deltaTime;
    }

}
