﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float maxMoveSpeed = 5f;

    private Rigidbody2D Rigidbody;

    private Vector2 velocity;

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Move(Camera.main.ScreenToWorldPoint(Input.mousePosition));
    }

    private void Move(Vector2 position)
    {
        Rigidbody.MovePosition(Vector2.SmoothDamp(transform.position, position, ref velocity, 1 / maxMoveSpeed));
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Collectible"))
        {
            UIManager.instance.CollectCoin();
            Destroy(other.gameObject);
        }
    }
}
