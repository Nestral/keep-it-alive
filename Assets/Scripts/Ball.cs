﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public int totalLives = 3;
    public float maxSpeed = 10f;

    public int currentLives = 3;

    private Rigidbody2D Rigidbody;
    private float velocity;
    private float targetPos;

    private void Awake()
    {
        currentLives = totalLives;
        Rigidbody = GetComponent<Rigidbody2D>();
        targetPos = transform.position.x - Camera.main.transform.position.x;
    }

    private void FixedUpdate()
    {
        Rigidbody.position = (new Vector2(Mathf.SmoothDamp(transform.position.x, targetPos + Camera.main.transform.position.x, ref velocity, 1 / maxSpeed)
            , transform.position.y));

        //float speed = Rigidbody.velocity.magnitude;
        //if (speed > maxSpeed)
        //    Rigidbody.velocity *= maxSpeed / speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Ground"))
        {
            currentLives--;
            UIManager.instance.LoseLife();
            if (currentLives <= 0)
            {
                Destroy(gameObject);
            }
            else
            {
                transform.position = new Vector2(-5, 3) + (Vector2)Camera.main.transform.position;
                Rigidbody.velocity = Vector2.zero;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Collectible"))
        {
            UIManager.instance.CollectCoin();
            Destroy(other.gameObject);
        }
    }

}
