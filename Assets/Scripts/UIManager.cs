﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject heartPrefab;

    public Transform livesHolder;

    public Text coinsText;

    public GameObject gameOverMenu;
    public Text currentScoreText;
    public Text bestScoreText;

    public int livesLeft;
    public int coins;

    private List<GameObject> hearts = new List<GameObject>();
    private string bestScoreKey = "bestScore";

    public static UIManager instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        livesLeft = 3;
        for (int i = 0; i < livesLeft; i++)
        {
            GameObject heart = Instantiate(heartPrefab, livesHolder);
            hearts.Add(heart);
        }
    }

    private void GameOver()
    {
        int bestScore = PlayerPrefs.GetInt(bestScoreKey, 0);
        if (coins > bestScore)
        {
            PlayerPrefs.SetInt(bestScoreKey, coins);
        }
        bestScoreText.text = $"Best score: {bestScore}";
        currentScoreText.text = $"Score: {coins}";

        Time.timeScale = 0f;
        gameOverMenu.SetActive(true);
    }

    public void RestartGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoseLife()
    {
        livesLeft--;
        hearts[livesLeft].SetActive(false);

        if (livesLeft <= 0)
            GameOver();
    }

    public void CollectCoin()
    {
        coins++;
        coinsText.text = coins.ToString();
    }

}
